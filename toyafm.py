from sklearn.linear_model import LogisticRegression
import numpy, random, sys, math, json

def make_training_data(obs_table, num_skills, num_students, skill):
        #obs_table: list of lists, each inner list is a trajectory of one student's interactions with problems from a skill
	#skill: the id of the skill that generated all of these observations

	features = []
        y = []

	# build AFM model input data
	# model has 1 parameter per student (ability)
	# 2 parameters per skill (skill difficulty, learning rate)
	num_feats_per_student = 1
	num_feats_per_skill = 2
	skillf = num_skills * num_feats_per_skill
	studentf = num_students * num_feats_per_student
	num_features = studentf + skillf
	
        for i, obs_list in enumerate(obs_table):
                for c, obs in enumerate(obs_list):
                	features.append([0] * num_features)
                	#first ten features are student abilities. Flag the student
                	features[-1][i] = 1
                        #shift over the students, and to the start of this skill
                        shift = studentf + skill * num_feats_per_skill
                        #flag the skill difficulty parameter
                        features[-1][shift] = 1
                        #Set this to number of problems seen by student
                        features[-1][shift + 1] = c
                        y.append(obs)
        return features, y



#load in the toy observations
toy_data = json.loads(open("toy_observations.json","r").readline())


# training data will be 14 * 200 matrix for X, 1 * 200 vector for Y
X = []
Y = []
x, y = make_training_data(toy_data['math'], 2, 10, 0)
X.extend(x)
Y.extend(y)
x, y = make_training_data(toy_data['science'], 2, 10, 1)
X.extend(x)
Y.extend(y)

model = LogisticRegression()
model.fit(X,Y)
print(model.intercept_)
print(model.coef_)




# load a skill from the real dataset
skill_data = json.loads(open("user_skill_data.json","r+").readline())
#make table for x axis
table = []
for user, skills in skill_data.iteritems():
	table.append([])
	table[-1] = [int(x) for x in skills["x axis"]]

x,y = make_training_data(table, 1, 69, 0)

model2 = LogisticRegression()
model2.fit(x,y)
print(model2.intercept_)
print(model2.coef_)



